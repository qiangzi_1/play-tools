### play向导插件使用说明

    
### 安装

以eclipse link方式安装插件

### 使用

 1. **配置play路径**：
       打开windows -> preferences -> PlayFramework ->
       填写本机play的可执行文件路径，window环境下配置为play.bat的文件路径；linux下配置为play的文件路径
       如果已经在环境变量PATH中配置了play的可执行路径，可以仅填上play可执行文件的名称。

 2. **创建play项目**：
   文件 -> 新建 -> Project 或 Others -> PlayFramework/Play project
   填写项目名称；
   可以选择自定义的项目上级目录（默认为当前workspace）；
   可以填写play项目的app名称
   可以选择将当前项目添加到workingset中。
   点击finish，play项目被创建并添加到workspace中；若当前不是java perspective，会提示是否切换perspective。
 
 3. **一键解决项目依赖**
     修改了项目的dependencies.yml文件之后，右键项目（或项目下某资源节点），选择“Play -> Resolve dependencies"
     即可自行执行以下一系列过程
     ```deps -> ec -> refresh project``` 

4  **安装Play模块**
        右键 -> "Play -> Install.." ,在弹出的对话框中填写要安装的play 模块名称，如"cobertura-1.0"，确定之后可开始安装。

5  **使用Play命令调试窗口**
        右键 -> "Play Commands..."，会弹出执行命令的对话框，可直接执行任何Play 内置命令，如start/stop/pid/status/list-modules等等。

_Tips：对于某些需要提供交互式输入的命令(比如install需要确认“是否安装该版本”)，可在“命令行输入”文本框中预填入用于输入的文本，如"y"
命令执行过程中的输出可通过Console视图[PlayFramework Console]查看到。_