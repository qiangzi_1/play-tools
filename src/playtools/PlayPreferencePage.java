package playtools;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class PlayPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public PlayPreferencePage(){
		super(GRID);
		this.setPreferenceStore(PlayTools.getDefault().getPreferenceStore());
	}
	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub
		
	}

	protected void createFieldEditors() {
		this.addField(new StringFieldEditor(PlayToolsConstants.PLAY_PATH, "play path: ", this.getFieldEditorParent()));
	}


}
