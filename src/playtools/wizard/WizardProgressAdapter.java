package playtools.wizard;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class WizardProgressAdapter implements IRunnableWithProgress{
    
	private IWorkspaceRunnable runnable;
	private ISchedulingRule rule;
	public WizardProgressAdapter(IWorkspaceRunnable runnable, ISchedulingRule rule){
		this.runnable = runnable;
		this.rule = rule;
	}
	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		try {
			ResourcesPlugin.getWorkspace().run(this.runnable, this.rule, IWorkspace.AVOID_UPDATE, monitor);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
