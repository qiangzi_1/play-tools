package playtools.wizard;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import playtools.PlayCommands;
import playtools.PlayTools;
import playtools.core.PlayOperator;
import playtools.util.ColorUtil;
import playtools.util.LayoutUtil;
import playtools.util.LogConsole;

public class CmdDialog extends Dialog implements ModifyListener{

    private String location;
    
    private Combo cmdText;
    private Label detailLabel;
    
    private Text argsText;
    private Text inputText;

    private Text locationText;
    
    public CmdDialog(Shell parentShell, String location) {
        super(parentShell);
        this.location = location;
        this.setShellStyle(this.getShellStyle()&(~SWT.APPLICATION_MODAL)|SWT.TOP);
    }
    
    protected Control createDialogArea(Composite parent) {
        getShell().setText("Play 命令行操作");
        Composite comp = initCmdComp(parent);
        LayoutUtil.gdb(comp, 1, 1);
        return comp;
    }

    private Composite initCmdComp(Composite parent){
        Composite comp = new Composite(parent, SWT.NONE);
        LayoutUtil.gl(comp, 2);
        
        new Label(comp, SWT.None).setText("位置: ");
        this.locationText = new Text(comp, SWT.BORDER|SWT.READ_ONLY);
        this.locationText.setText(this.location);
        LayoutUtil.gdh(locationText);
        
        new Label(comp, SWT.None).setText("命令: ");
        this.cmdText = new Combo(comp, SWT.BORDER);
        LayoutUtil.gdh(cmdText);
        installCmds();
        
        new Label(comp, SWT.None).setText("说明: ");
        detailLabel = new Label(comp, SWT.None);
        detailLabel.setForeground(ColorUtil.blue);
        detailLabel.setText("[命令说明]");
        LayoutUtil.gdh(this.detailLabel);
        
        new Label(comp, SWT.None).setText("参数: ");
        argsText = new Text(comp, SWT.BORDER);
        LayoutUtil.gdh(argsText);
        
        Label l = new Label(comp, SWT.None);
        l.setText("命令行输入: ");
        l.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
        inputText = new Text(comp, SWT.BORDER|SWT.MULTI);
        inputText.setToolTipText("在执行命令时需要提供交互性输入的情况下使用，" +
        		"\n如 install 命令需要输入'y'");
        LayoutUtil.gdb(inputText, 1, 1);
        
        new Label(comp, SWT.None);
        new Label(comp, SWT.None).setText("可在Console视图中查看命令行输出");
        this.cmdText.addModifyListener(this);
        return comp;
    }
    
    private void installCmds() {
        for(String cmd: PlayCommands.cmdKeys()){
            this.cmdText.add(cmd);
        }
    }

    protected Point getInitialSize() {
        return new Point(520, 400);
    }
    
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, "执行命令",
                true);
        createButton(parent, IDialogConstants.CANCEL_ID, "关闭窗口", false);
    }
    
    public void modifyText(ModifyEvent e) {
        String cmd = this.cmdText.getText().trim();
        if(cmd.length() == 0){
            this.detailLabel.setText("[未输入命令]");
            return;
        }
        String detail = PlayCommands.getDescription(cmd);
        if(detail == null)
            detail = "[未知命令]";
        this.detailLabel.setText(detail);
    }
    
    protected void okPressed(){
        final String cmd = this.cmdText.getText().trim();
        if(cmd.length() == 0)
            return;
        final String args = this.argsText.getText().trim();
        final String input = inputText.getText();
        LogConsole.getInstance().clearConsole();
        WorkspaceJob job = new WorkspaceJob("执行命令:" + cmd) {
            public IStatus runInWorkspace(IProgressMonitor monitor)
                    throws CoreException {
                monitor.beginTask("执行命令:" + cmd, 1);
                monitor.setTaskName(String.format("%s %s %s --[%s] with input(%s)", 
                		PlayTools.getPlayPath(), cmd, args, location, input));
                PlayOperator.INSTANCE.invokeProcByInput(new String[]{PlayTools.getPlayPath(), 
                        cmd, args, }, location, input);
                monitor.worked(1);
                monitor.done();
                return Status.OK_STATUS;
            }
        };
        
        job.schedule();
    }

    
}
