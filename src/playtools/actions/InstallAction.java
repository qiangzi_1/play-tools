package playtools.actions;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import playtools.PlayTools;
import playtools.core.PlayOperator;
import playtools.util.LogConsole;

public class InstallAction implements IObjectActionDelegate {
    
    public InstallAction() {
    }
    
    public void run(IAction action) {
        InputDialog dlg = new InputDialog(null, "安装Play模块",
                "输入要安装的模块(如cobertura-1.0)", null,
                new IInputValidator(){
                    public String isValid(String newText) {
                        if( newText.trim().length() == 0 )
                            return "请输入模块名称[-版本号]";
                        return null;
                    }
            
        });
        if(dlg.open() != Window.OK)
            return;
        final String module = dlg.getValue().trim();
        LogConsole.getInstance().clearConsole();
        
        WorkspaceJob job = new WorkspaceJob("安装Play模块:" + module) {
            public IStatus runInWorkspace(IProgressMonitor monitor)
                    throws CoreException {
                monitor.beginTask("安装Play模块:" + module, 1);
                
                monitor.setTaskName(String.format("%s install %s...", 
                		PlayTools.getPlayPath(), module));
                PlayOperator.INSTANCE.execute("install", module);
                monitor.worked(1);
                monitor.done();
                return Status.OK_STATUS;
            }
        };
        
        job.schedule();
    }
    
    public void selectionChanged(IAction action, ISelection selection) {
    }
    
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
    }
    
}