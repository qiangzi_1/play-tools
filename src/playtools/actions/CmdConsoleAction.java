package playtools.actions;

import java.io.File;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import playtools.util.UIUtils;
import playtools.wizard.CmdDialog;

public class CmdConsoleAction implements IObjectActionDelegate {
    
    private IResource resource;
    
    public CmdConsoleAction() {
        
    }
    
    public void run(IAction action) {
        if (this.resource == null)
            return;
        final IProject project = resource.getProject();
        if (project == null)
            return;
        
        final File file = project.getLocation().makeAbsolute().toFile();
        new CmdDialog(UIUtils.getGlobalShell(), file.getAbsolutePath()).open();
    }
    
    public void selectionChanged(IAction action, ISelection selection) {
        if (selection instanceof IStructuredSelection) {
            Object obj = ((IStructuredSelection)selection).getFirstElement();
            if (obj != null && obj instanceof IResource) {
                resource = (IResource)obj;
            }
        }
    }
    
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
    }
    
}

