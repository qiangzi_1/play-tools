package playtools;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PlayCommands {
    public static Map<String, String> cmdMap = new TreeMap<String, String>();
    static{
    cmdMap.put("antify", "Create a build.xml file for this project");
    cmdMap.put("auto-test", "Automatically run all application tests");
    cmdMap.put("build-module", "Build and package a module");
    cmdMap.put("check", "Check for a release newer than the current one");
    cmdMap.put("classpath", "Display the computed classpath");
    cmdMap.put("clean", "Delete temporary files (including the bytecode cache)");
    cmdMap.put("deps", "Resolve and retrieve project dependencies");
    cmdMap.put("dependencies", "Resolve and retrieve project dependencies");
    cmdMap.put("ec", "Create all Eclipse configuration files");
    cmdMap.put("eclipsify", "Create all Eclipse configuration files");
    cmdMap.put("evolutions", "Run the evolution check");
    cmdMap.put("evolutions:applyAutomatically", "apply pending evolutions");
    cmdMap.put("evolutions:markAppliedMark", "pending evolutions as manually applied");
    cmdMap.put("evolutions:resolveResolve", "partially applied evolution");
    cmdMap.put("help", "Display help on a specific command");
    cmdMap.put("id", "Define the framework ID");
    cmdMap.put("idealize", "Create all IntelliJ Idea configuration files");
    cmdMap.put("install", "Install a module");
    cmdMap.put("javadoc", "Generate your application Javadoc");
    cmdMap.put("list-modules", "List modules available from the central modules repositoy");
    cmdMap.put("modules", "Display the computed modules list");
    cmdMap.put("netbeansify", "Create all NetBeans configuration files");
    cmdMap.put("new", "Create a new application");
    cmdMap.put("new-module", "Create a module");
    cmdMap.put("out", "Follow logs/system.out file");
    cmdMap.put("pid", "Show the PID of the running application");
    cmdMap.put("precompile", "Precompile all Java sources and templates to speed up application on start-up");
    cmdMap.put("restart", "Restart the running application");
    cmdMap.put("run", "Run the application in the current shell");
    cmdMap.put("secret", "Generate a new secret key");
    cmdMap.put("start", "Start the application in the background");
    cmdMap.put("status", "Display the running application's status");
    cmdMap.put("stop", "Stop the running application");
    cmdMap.put("test", "Run the application in test mode in the current shell");
    cmdMap.put("version", "Print the framework version");
    cmdMap.put("war", "Export the application as a standalone WAR archive");
    }
    
    public static List<String> cmdKeys(){
        return new ArrayList<String>(cmdMap.keySet());
    }
    
    public static String getDescription(String key){
        return cmdMap.get(key);
    }
}
