package playtools;

import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class PlayTools extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "playtools"; //$NON-NLS-1$

	// The shared instance
	private static PlayTools plugin;
	
	/**
	 * The constructor
	 */
	public PlayTools() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		plugin.getPreferenceStore().setDefault(PlayToolsConstants.PLAY_PATH, "play.bat");
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static PlayTools getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static String getPlayPath(){
		return plugin.getPreferenceStore().getString(PlayToolsConstants.PLAY_PATH);
	}
	
	public static String checkPlayPath(){
		String playCmd = getPlayPath();
		if(playCmd == null || playCmd.trim().length() == 0) return "未配置Play path";
		//File f = new File(playCmd);
		//if(!f.isFile()){
		//	return "Play path 配置项无效";
		//}
		return null;
	}
	
	public static IWorkspace getWorkspace(){
		return ResourcesPlugin.getWorkspace();
	}

	public static IResourceRuleFactory  getResourceRuleFactory(){
		return getWorkspace().getRuleFactory();
	}
}
