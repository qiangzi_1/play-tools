package playtools.util;

import java.io.IOException;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;


public class LogConsole extends MessageConsole{

	private MessageConsoleStream infoConsoleStream = null ;
	private MessageConsoleStream errConsoleStream = null;
	
	private LogConsole(String consoleName, ImageDescriptor imageDescriptor) {
		super(consoleName, imageDescriptor);
		infoConsoleStream = this.newMessageStream();
		errConsoleStream = this.newMessageStream();
		infoConsoleStream.setColor(ColorUtil.getColor(SWT.COLOR_BLACK));
		errConsoleStream.setColor(ColorUtil.getColor(SWT.COLOR_RED));
		
		IConsoleManager mgr = ConsolePlugin.getDefault().getConsoleManager();
		mgr.addConsoles(new IConsole[] { this });
		mgr.showConsoleView(this);
	}
	
	public MessageConsoleStream getErrConsoleStream() {
		return errConsoleStream;
	}

	public MessageConsoleStream getInfoConsoleStream() {
		return infoConsoleStream;
	}
	
	public void info(String message){
		try {
			this.infoConsoleStream.write(message + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void error(String message) {
		try {
			this.errConsoleStream.write(message + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static LogConsole getInstance(){
		return instance;
	}
	
	private static final LogConsole instance = new LogConsole("Play Framework Console", null);
}
