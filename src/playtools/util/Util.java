package playtools.util;

import java.util.Map;

public class Util {
	/**
     * Converts the map format of the environment variables to the K=V format in the array.
     */
    public static String[] mapToEnv(Map<String,String> m) {
        String[] r = new String[m.size()];
        int idx=0;

        for (final Map.Entry<String,String> e : m.entrySet()) {
            r[idx++] = e.getKey() + '=' + e.getValue();
        }
        return r;
    }
}
