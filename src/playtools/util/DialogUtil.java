package playtools.util;

import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.DirectoryDialog;

public class DialogUtil {

	public static String openFolderDialog(String title, String message){
		DirectoryDialog dlg = new DirectoryDialog(UIUtils.getGlobalShell());
        dlg.setText(title);
        dlg.setMessage(message);
        String folder = dlg.open();
        return folder;
	}
	
	public static RGB openColorDialog(String text){
		ColorDialog dlg = new ColorDialog(UIUtils.getGlobalShell());
		if(text != null) dlg.setText(text);
		RGB rgb = dlg.open();
		return rgb;
	}
}
