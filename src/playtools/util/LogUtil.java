package playtools.util;

import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.swt.widgets.Display;


public class LogUtil {
	
	public static void info(final byte[] bytes, final int offset, final int len){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				try {
					LogConsole.getInstance().getInfoConsoleStream().write(bytes, offset, len);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void error(final byte[] bytes, final int offset, final int len){
        Display.getDefault().asyncExec(new Runnable(){
            public void run(){
                try {
                    LogConsole.getInstance().getErrConsoleStream().write(bytes, offset, len);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
	
	public static void info(final String message){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				LogConsole.getInstance().info(message);
			}
		});
		
	}
	
	public static void error(final Exception e){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				e.printStackTrace(new PrintStream(LogConsole.getInstance().getErrConsoleStream()));
			}
		});
	}

	public static void error(final String message) {
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				LogConsole.getInstance().error(message);
			}
		});
	}
	
	
}
