package playtools.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

public class ColorUtil {
	public static Font sysFont = Display.getDefault().getSystemFont();
	public static Color white = new Color(null, 255, 255, 255);
	public static Color black= new Color(null, 0, 0, 0);
	public static Color gray= new Color(null, 168, 173, 179);
	public static Color blue= new Color(null, 0, 0, 255);
	public static Color textBorderHover = new Color(null, 220, 220, 220); //高亮外框
	public static Color textBorder = new Color(null, 50, 50, 50); //普通边框
	public static Color triangleBack1 = new Color(null, 220, 220, 220);
	public static Color triangleBack2 = gray;
	public static Color triangleBorder = gray;
	public static Color triangleColor = black;//new Color(null, 7, 124, 168);
	public static Color toolInfo = new Color(null, 120, 120, 120);
	public static Color getColor(int id){
		return Display.getDefault().getSystemColor(id);
	}
	
	public static Color getToolTipColor(){
		return Display.getDefault().getSystemColor(SWT.COLOR_INFO_BACKGROUND);
	}
	
	public static Color getColor(int r, int g, int b){
		return new Color(Display.getDefault(), r, g, b);
	}

	public static Image getScaledImage(Image source, int width, int height){
		ImageData data = source.getImageData();
		data = data.scaledTo(width, height);
		return new Image(null,data);
	}
}
