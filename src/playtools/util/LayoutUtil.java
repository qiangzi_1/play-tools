package playtools.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class LayoutUtil {
	public static void centerShell(Display display, Shell shell) {
		Rectangle displayBounds = display.getPrimaryMonitor().getBounds();
		Rectangle shellBounds = shell.getBounds();
		int x = displayBounds.x + (displayBounds.width - shellBounds.width) >> 1;
		int y = displayBounds.y + (displayBounds.height - shellBounds.height) >> 1;
		shell.setLocation(x, y);
	}
	public static void centerShell(Shell shell){
		centerShell(shell.getDisplay(), shell);
	}
	
	public static void fl(Composite comp, int mw, int mh){
		comp.setLayout(newFillLayout(mw, mh));
	}
	/**
	 * 对容器进行简易布局，间隔使用默认
	 * @param comp
	 * @param col
	 */
	public static void gl(Composite comp, int col){
		comp.setLayout(newGridLayout(col));
	}
	public static void gl(Composite comp, int col, int mw, int mh){
		comp.setLayout(newGridLayout(col, mw, mh));
	}
	public static void gl(Composite comp, int col, int mw, int mh, int hspace, int vspace){
		comp.setLayout(newGridLayout(col, mw, mh, hspace, vspace));
	}
	/**
	 * 对容器进行最佳布局，间隔使用较佳大小
	 * @param comp
	 * @param col
	 */
	public static void glPrefered(Composite comp, int col){
		comp.setLayout(newGridLayout(col, 20, 20, 6, 10));
	}
	public static void glPreferedForSingleLine(Composite comp, int col){
		comp.setLayout(newGridLayout(col, 30, 20, 6, 10));
	}
	/**
	 * 为左边标签统一进行右对齐
	 * @param labels
	 */
	public static void gdLeftLabels(Control[] labels){
		if(labels != null){
			for(Control control: labels){
				gdLeftLabel(control);
			}
		}
	}
	public static void gdLeftLabel(Control label){
	GridData data = new GridData(GridData.HORIZONTAL_ALIGN_END);
	label.setLayoutData(data);
	}
	/**
	 * 为控件组统一设置水平抢占充满
	 * @param controls
	 * @param hspan
	 */
	public static void gdh(Control[] controls, int hspan){
		if(controls != null){
			for(Control control: controls){
				control.setLayoutData(newHSpanGridData(hspan));
			}
		}
	}
	public static void gdh(int hspan, Control... controls){
		gdh(controls, hspan);
	}
	
	public static void gdh(Control control, int hspan){
		control.setLayoutData(newHSpanGridData(hspan));
	}
	public static void gdh(Control control){
		control.setLayoutData(newHSpanGridData(1));
	}
	
	public static void gdb(Control control, int hspan, int vspan){
		control.setLayoutData(newBothSpanGridData(hspan, vspan));
	}
	public static void gd_btn(Button b, int wHint, int hHint){
		GridData gd = new GridData();
		gd.heightHint = hHint;
		gd.widthHint = wHint;
		b.setLayoutData(gd);
	}
	public static void gd_btn(Button b){
		GridData gd = new GridData();
		gd.heightHint = 20;
		b.setLayoutData(gd);
	}
	
	public static GridLayout newGridLayout(int col){
		GridLayout layout = new GridLayout(col, false);
		return layout;
	}
	public static GridLayout newGridLayout(int col, int mw, int mh){
		GridLayout layout = new GridLayout(col, false);
		layout.marginHeight = mh;
		layout.marginWidth = mw;
		return layout;
	}
	public static GridLayout newGridLayout(int col, int mw, int mh, int hs, int vs){
		GridLayout layout = new GridLayout(col, false);
		layout.marginHeight = mh;
		layout.marginWidth = mw;
		layout.horizontalSpacing = hs;
		layout.verticalSpacing = vs;
		return layout;
	}
	public static FillLayout newFillLayout(int mw, int mh){
		FillLayout layout = new FillLayout();
		layout.marginWidth = mw;
		layout.marginHeight = mh;
		return layout;
	}
	public static GridData newBothSpanGridData(int hspan, int vspan){
		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = hspan;
		data.verticalSpan = vspan;
		return data;
	}
	public static GridData newHSpanGridData(int span){
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = span;
		return data;
	}
	public static GridData newVSpanGridData(int span){
		GridData data = new GridData(GridData.FILL_VERTICAL);
		data.verticalSpan = span;
		return data;
	}
	public static void addHorizontalSeperator(Composite parent, int span){
		LayoutUtil.gdh(new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL), span);
	}
}
