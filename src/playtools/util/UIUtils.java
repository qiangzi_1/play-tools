package playtools.util;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;

import playtools.PlayTools;

public class UIUtils {

	public static Shell getGlobalShell(){
		return PlayTools.getDefault().getWorkbench().getActiveWorkbenchWindow().getShell();
	}
	
	public static IWorkbenchWindow getGlobalWindow(){
		return PlayTools.getDefault().getWorkbench().getActiveWorkbenchWindow();
	}
}
