package playtools;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;



/**
 * 图像管理类，对系统用到的图片进行同一管理
 * @author Atp
 */
public class ImagesContext {
	//图标文件夹路径
	public final static String ICONS_PATH ="icons/";
	private static ImageRegistry imageRegistry;
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
			declareImages();
		}
		
		return imageRegistry;
	}
	
	private static void declareImages() {
	}

	
	@SuppressWarnings("unused")
    private static void declareRegistryImage(String key, String path) {
		try {
			URL url= FileLocator.find(PlayTools.getDefault().getBundle(), new Path(path), null);
			if (url != null) {
				ImageDescriptor image = ImageDescriptor.createFromURL(url);
				getImageRegistry().put(key, image);
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	
	public static String getImageFile(String fileName ){
		    String path = ICONS_PATH + fileName;
			URL url= FileLocator.find(PlayTools.getDefault().getBundle(), new Path(path), null);
			
			if(url != null){
				try {
					URL fileURL = FileLocator.toFileURL(url);
					if(fileURL != null){
						return fileURL.getFile();
					}
				} catch (IOException e) {}
			}
			return null;
	}
	
	public static Image getImage(String key){
		return getImageRegistry().get(key);
	}
	
	public static ImageDescriptor getImageDescriptor(String key){
		return  getImageRegistry().getDescriptor(key);
	}
}